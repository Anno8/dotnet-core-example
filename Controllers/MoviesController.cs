﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MoviesBackend.Models;
using MoviesBackend.Repositories;

namespace MoviesBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieRepository _movieRepository;

        public MoviesController(IMovieRepository movieRepository) =>
            _movieRepository = movieRepository;

        // GET api/movies
        [HttpGet]
        public ActionResult<IEnumerable<Movie>> Get() =>
            _movieRepository.Get()?.ToList();

        // GET api/movies/5
        [HttpGet("{id}")]
        public ActionResult<Movie> Get(int id) =>
            _movieRepository.Get(id);

        // POST api/movies
        [HttpPost]
        public void Post([FromBody] Movie movie) =>
            _movieRepository.Create(movie);

        // PUT api/movies/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Movie movie) =>
            _movieRepository.Update(movie);

        // DELETE api/movies/5
        [HttpDelete("{id}")]
        public void Delete(int id) =>
            _movieRepository.Delete(id);
    }
}

﻿namespace MoviesBackend.Models
{
    public class Movie
    {
        // Primary auto incremented key for the movie table
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

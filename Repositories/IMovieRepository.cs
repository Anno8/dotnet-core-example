﻿using System.Collections.Generic;
using MoviesBackend.Models;

namespace MoviesBackend.Repositories
{
    public interface IMovieRepository
    {
        Movie Get(int id);
        IEnumerable<Movie> Get();
        Movie Create(Movie movie);
        Movie Update(Movie movie);
        void Delete(int id);
    }
}

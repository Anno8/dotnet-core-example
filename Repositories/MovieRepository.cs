﻿using System.Collections.Generic;
using System.Linq;
using MoviesBackend.Models;

namespace MoviesBackend.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly DatabaseContext _dbContext;

        public MovieRepository(DatabaseContext dbContext) =>
            _dbContext = dbContext;

        public Movie Get(int id) =>
            _dbContext.Movies.FirstOrDefault(x => x.Id == id);

        public IEnumerable<Movie> Get() =>
            _dbContext.Movies.AsEnumerable();

        public Movie Create(Movie movie)
        {
            var result = _dbContext.Movies.Add(movie);
            _dbContext.SaveChanges();
            return result.Entity;
        }

        public Movie Update(Movie movie)
        {
            _dbContext.Movies.Update(movie);
            _dbContext.SaveChanges();
            return movie;
        }

        public void Delete(int id)
        {
            if (!_dbContext.Movies.Any(e => e.Id == id))
                return;

            _dbContext.Movies.Remove(new Movie { Id = id });
            _dbContext.SaveChanges();
        }
    }
}
